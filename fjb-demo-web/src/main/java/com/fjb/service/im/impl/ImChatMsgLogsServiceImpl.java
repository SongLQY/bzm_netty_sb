package com.fjb.service.im.impl;

import com.fjb.pojo.im.ImChatMsgLogs;
import com.fjb.service.im.ImChatMsgLogsService;
import com.fjb.enums.im.ImChatVersion;import com.fjb.enums.im.ImDataSources;
import com.fjb.enums.im.ImMsgReadStatus;
import com.fjb.enums.im.ImMsgType;
import com.fjb.mapper.im.ImChatMsgLogsMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * im_chat聊天记录 服务实现类
 * </p>
 *
 * @author hemiao
 * @since 2020-05-26	
 */
@Service
public class ImChatMsgLogsServiceImpl extends ServiceImpl<ImChatMsgLogsMapper, ImChatMsgLogs> implements ImChatMsgLogsService {
	
	@Autowired
	private ImChatMsgLogsMapper imChatMsgLogsMapper;
	
	@Override
	public Integer saveWebMsgLogs(ImChatMsgLogs logs) {
		LocalDateTime dateTime = LocalDateTime.now();
		logs.setCreateTime(dateTime);
		logs.setSendTime(dateTime);	
		logs.setMsgType(ImMsgType.TYPE_1.getCode());
		logs.setMsgReadStatus(ImMsgReadStatus.STATUS_2.getCode());
		logs.setChatVersion(ImChatVersion.VSERSION.getCode());
		logs.setDataSources(ImDataSources.S_1000.getCode());
		imChatMsgLogsMapper.insert(logs);
		return logs.getId();
	}

	@Override
	public Integer updateOfflineStatusTwo(Integer msgId) {
		ImChatMsgLogs logs = new ImChatMsgLogs();
		logs.setId(msgId);
		logs.setMsgOfflineStatus(2);
		imChatMsgLogsMapper.updateById(logs);
		return msgId;
	}

	@Override
	public void updateMsgReadStatusOne(List<String> msgIdList) {
		ArrayList<ImChatMsgLogs> batchList = new ArrayList<ImChatMsgLogs>();
		LocalDateTime localDateTime = LocalDateTime.now();
		for (String msg : msgIdList) {
			ImChatMsgLogs logs = new ImChatMsgLogs();
			logs.setId(Integer.valueOf(msg));
			logs.setMsgReadStatus(ImMsgReadStatus.STATUS_1.getCode());
			logs.setReceiveTime(localDateTime);
			batchList.add(logs);
		}
		saveOrUpdateBatch(batchList);
	}
	
	
	
}
