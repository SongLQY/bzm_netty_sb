package com.fjb.service.im.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fjb.mapper.im.ImChatFriendGroupMapper;
import com.fjb.pojo.im.ImChatFriendGroup;
import com.fjb.service.im.ImChatFriendGroupService;

/**
 * <p>
 * 朋友组 服务实现类
 * </p>
 *
 * @author hemiao
 * @since 2020-06-02
 */
@Service
public class ImChatFriendGroupServiceImpl extends ServiceImpl<ImChatFriendGroupMapper, ImChatFriendGroup> implements ImChatFriendGroupService {

}
