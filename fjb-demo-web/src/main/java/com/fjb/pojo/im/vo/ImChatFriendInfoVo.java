package com.fjb.pojo.im.vo;

import com.fjb.pojo.im.ImChatFriendInfo;

/**
 * @Description:朋友信息vo
 * @author hemiao
 * @time:2020年6月3日 下午1:07:58
 */
public class ImChatFriendInfoVo extends ImChatFriendInfo{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1617743742384287200L;

	private String nickname;
	
	private String phone;
	
	private String gender;

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
}
