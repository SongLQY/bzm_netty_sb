package com.fjb.controller.im;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 朋友组 前端控制器
 * </p>
 *
 * @author hemiao
 * @since 2020-06-02
 */
@Controller
@RequestMapping("/imChatFriendGroup")
public class ImChatFriendGroupController {

}
