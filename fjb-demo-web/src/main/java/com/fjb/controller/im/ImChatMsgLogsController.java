package com.fjb.controller.im;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fjb.entity.HttpCode;
import com.fjb.entity.JsonResult;
import com.fjb.pojo.im.ImChatMsgLogs;
import com.fjb.service.im.ImChatMsgLogsService;

/**
 * <p>
 * im_chat聊天记录 前端控制器
 * </p>
 *
 * @author hemiao
 * @since 2020-05-26
 */
@Controller
@RequestMapping("/imChatMsgLogs")
public class ImChatMsgLogsController {
	
	@Autowired
	private ImChatMsgLogsService imChatMsgLogsService;
	
	// 查询好友聊天记录
	@RequestMapping(value="/selectFriendLogsList")
	@ResponseBody
	public JsonResult<Object> selectFriendLogsList(HttpServletRequest request){
		HttpCode httpCode = HttpCode.PARAM_VERIFICATION;
		String sendId = request.getParameter("sendId");
		String receiveId = request.getParameter("receiveId");
		if(StringUtils.isBlank(sendId)||StringUtils.isBlank(receiveId)) {
			return new JsonResult<Object>(null, httpCode);
		}		
		QueryWrapper<ImChatMsgLogs> queryWrapper = new QueryWrapper<ImChatMsgLogs>();
		queryWrapper.eq("to_type", 1);		
		//queryWrapper.eq("send_id", sendId).or().eq("receive_id", sendId);
		queryWrapper.and(wrapper -> wrapper.eq("receive_id", receiveId).or().eq("send_id", receiveId));
		queryWrapper.and(wrapper -> wrapper.eq("send_id", sendId).or().eq("receive_id", sendId));
		//queryWrapper.eq("receive_id", receiveId).or().eq("send_id", receiveId);
		List<ImChatMsgLogs> infoList = imChatMsgLogsService.list(queryWrapper);
		return new JsonResult<Object>(infoList, HttpCode.SUCCESS);
	}
	
	// 查询群聊天记录
	@RequestMapping(value="/selectGroupLogsList")
	@ResponseBody
	public JsonResult<Object> selectGroupLogsList(HttpServletRequest request,Integer groupInfoId){
		HttpCode httpCode = HttpCode.PARAM_VERIFICATION;
		if(groupInfoId==null) {
			return new JsonResult<Object>(null, httpCode);
		}
		QueryWrapper<ImChatMsgLogs> queryWrapper = new QueryWrapper<ImChatMsgLogs>();
		queryWrapper.eq("group_info_id", groupInfoId);
		queryWrapper.eq("data_status", 1);
		List<ImChatMsgLogs> infoList = imChatMsgLogsService.list(queryWrapper);
		return new JsonResult<Object>(infoList, HttpCode.SUCCESS);
	}
}
