package com.fjb.controller.im;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fjb.base.BaseController;
import com.fjb.entity.HttpCode;
import com.fjb.entity.JsonResult;
import com.fjb.pojo.user.SysUser;
import com.fjb.pojo.user.vo.ImChatUserVo;
import com.fjb.service.im.ImChatService;
import com.fjb.service.user.SysUserService;
import com.fjb.utils.NetworkUtil;

/**
 * @Description:即时通讯聊天
 * @author hemiao
 * @time:2020年5月5日 下午3:32:38
 */
@Controller
@RequestMapping("/imChat")
public class ImChatControler extends BaseController{
	
	@Autowired
	private ImChatService imChatService;
	@Autowired
	private SysUserService sysUserService;
	
	/**
	 * @Description:显示通聊天
	 * @param request
	 * @return
	 * ModelAndView
	 * @exception:
	 * @author: hemiao
	 * @time:2020年5月5日 下午4:50:34
	 */
	@RequestMapping(value="/showImChatInfo")
    public ModelAndView showImChatInfo(HttpServletRequest request) {
    	ModelAndView mv = new ModelAndView();	
    	mv.setViewName("/im/im_chat_info");
    	return mv;
    }
	
	/**
	 * @Description:获得登录用户信息
	 * @param request
	 * @return
	 * JsonResult<SysUser>
	 * @exception:
	 * @author: hemiao
	 * @time:2020年5月27日 上午10:37:20
	 */
	@RequestMapping(value="/login")
	@ResponseBody
	public JsonResult<ImChatUserVo> login(HttpServletRequest request){
		HttpCode httpCode = HttpCode.PARAM_VERIFICATION;
		String phone = request.getParameter("phone");
		if(StringUtils.isBlank(phone)) {
			httpCode.setMsg("手机号不为空");
			return new JsonResult<ImChatUserVo>(null, httpCode);
		}
		String password = request.getParameter("password");
		if(StringUtils.isBlank(password)) {
			httpCode.setMsg("密码不为空");
			return new JsonResult<ImChatUserVo>(null, httpCode);
		}
		QueryWrapper<SysUser> userWrapper = new QueryWrapper<SysUser>();
		userWrapper.eq("phone", phone);
		SysUser sysUser = sysUserService.getOne(userWrapper);
		if(sysUser==null) {
			httpCode.setMsg("该用户信息不存在");
			return new JsonResult<ImChatUserVo>(null, httpCode);
		}
		String password2 = sysUser.getPassword();
		if(!password.equals(password2)) {
			httpCode.setMsg("账号或密码错误");
			return new JsonResult<ImChatUserVo>(null, httpCode);
		}
		ImChatUserVo chatUserVo = new ImChatUserVo();
		chatUserVo.setUser(sysUser);
		String ipAddress = NetworkUtil.getIpAddress(request);
		chatUserVo.setLoginIpLocation(ipAddress);		
		return new JsonResult<ImChatUserVo>(chatUserVo, HttpCode.SUCCESS);
	}
	
	/**
	 * @Description:发送消息
	 * @param request
	 * @return
	 * JsonResult<Object>
	 * @exception:
	 * @author: hemiao
	 * @time:2020年5月5日 下午9:23:41
	 */
	@RequestMapping(value="/sendMsg")
	@ResponseBody
	public JsonResult<Object> sendMsg(HttpServletRequest request){
		
		
		return new JsonResult<Object>(null, HttpCode.SUCCESS);
	}
	
	
	
}
