package com.fjb.tool.bio.block;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;

/**
 * @Description:bio 阻塞服务端
 * @author hemiao
 * @time:2020年4月14日 下午10:40:15
 */
public class BIoSingleServer {
	
	public static void main(String[] args) {
		String QUIT = "quit";
		int port = 8089;
		ServerSocket serverSocket = null;
		try {
			// 绑定监听端口
			serverSocket = new ServerSocket(port);
			System.out.println(" ServerSocket 启动成功  监听端口 port" + port);
			while (true) {	
				// 等待客户端连接	
				Socket socket = serverSocket.accept();
				int cPort = socket.getPort();
				System.out.println(" 客户端   连接 cPort " + cPort);
				// 获得输入字节流
				InputStream inputStream = socket.getInputStream();
				// 将输入字节流转成 输入字符流
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				// 将 字符流 变成 带缓冲区的 字符流 提前将数据保存到内存中 在内存中操作 速度更快
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				
				// 获得输出字节流
				OutputStream outputStream = socket.getOutputStream();
				// 将输出字节流 转成 输出字符流
				OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
				// 将 输出字符流 变成带缓冲区的输出字符流 ，提前将数据保存到内存中 在内存中操作 速度更快
				BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
						
				// 读取客户端发送的消息	
				String readLineMsg = null;
				// readLine()是一个阻塞函数，当没有数据读取时，就一直会阻塞在那，而不是返回null；一个办法是发送完数据后就关掉流，这样readLine()结束阻塞状态
				while((readLineMsg = bufferedReader.readLine()) != null) {
					System.out.println(" 客户端 cPort "+cPort+"   发送的消息是  " + readLineMsg);
					// 回复客户发送的消息	
					bufferedWriter.write(" 服务器回复的消息   = "+readLineMsg + "\n");
					bufferedWriter.flush();	
					
					if(readLineMsg.toString().equals(QUIT)) {
						System.out.println(" 客户端 cPort "+cPort+" 退出连接 ");
						break;
					}
				}
				
				//对方法数据给我了，读 Input
//				InputStream is = client.getInputStream();
//				//网络客户端把数据发送到网卡，机器所得到的数据读到了JVM内中
//				byte [] buff = new byte[1024];
//				int len = is.read(buff);
//				if(len > 0){
//					String msg = new String(buff,0,len);
//					System.out.println("收到" + msg);
//				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(serverSocket!=null) {
				try {
					// 关闭服务器
					serverSocket.close();
					System.out.println(" 关闭服务端 serverSocket ");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
		
	}
	
}
